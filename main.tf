##################################
####### create SonarQube #########
##################################

module "sonarqube" {
  source          = "git::https://github.com/devops-terraform-aws/ec2-instance-module.git?ref=v1.0.0"
  ami             = data.aws_ami.ubuntu-linux-2004.id
  instance_type   = "t2.medium"
  key_name        = module.aws_key.get_key_name
  name            = "sonarqube"
  region          = "us-east-2"
  security_groups = module.security_groups.security_name
  user_data       = file("${path.module}/scripts/sonarqube.sh")
}

##################################
######### create Nexus ###########
##################################

module "nexus" {
  source          = "git::https://github.com/devops-terraform-aws/ec2-instance-module.git?ref=v1.0.0"
  ami             = data.aws_ami.redhat-linux.id
  instance_type   = "t2.medium"
  key_name        = module.aws_key.get_key_name
  name            = "nexus"
  region          = "us-east-2"
  security_groups = module.security_groups.security_name
  user_data       = file("${path.module}/scripts/nexus.sh")
}

#################################
########### PEM key #############
#################################

module "aws_key" {
  source   = "git::https://github.com/devops-terraform-aws/ssh-key-module.git?ref=v1.0.0"
  key_name = "${var.key_name}-${module.unique_name.unique}"
}

module "unique_name" {
  source = "git::https://github.com/devops-terraform-aws/random-module.git?ref=v1.0.0"
}

resource "terraform_data" "generated_key" {
  provisioner "local-exec" {
    command = <<-EOT
        echo '${module.aws_key.private_key}' > ./'${var.key_name}-${module.unique_name.unique}'.pem
        chmod 400 ./'${var.key_name}-${module.unique_name.unique}'.pem
      EOT
  }
}

##################################
######### Security Groups ########
##################################
module "security_groups" {
  source      = "git::https://github.com/devops-terraform-aws/security-group-module.git?ref=v1.0.0"
  cidr_blocks = ["0.0.0.0/0"]
  name        = "security-group"
}