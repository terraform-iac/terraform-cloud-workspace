provider "aws" {
  region     = "us-east-2"
  # access_key = var.AWS_ACCESS_KEY_ID
  # secret_key = var.AWS_SECRET_ACCESS_KEY
}

terraform {
  required_version = ">= 1.3.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.20.0"
    }
  }

  cloud {
    organization = "devop-accelerator"

    workspaces {
      name = "virtual-machine"
    }
  }
}